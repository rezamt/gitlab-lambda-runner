module gitlab.com/msvechla/gitlab-lambda-runner

require (
	github.com/aws/aws-lambda-go v1.8.2
	github.com/namsral/flag v1.7.4-pre
	github.com/sirupsen/logrus v1.3.0
)
