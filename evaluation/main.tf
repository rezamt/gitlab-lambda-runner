provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "my-tf-test-lets-seeifitworks"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}